const fs = require('fs');
const jimp= require("jimp");

const UPLOAD_PATH = 'pictures/';

function moveUploadedPhoto(file, owner) {
    var savedName;
    let type = file.originalFilename.substr(file.originalFilename.length - 4, 4);
    //console.log(type)
    do {
        savedName = `photo_${owner}`; 
    } while( fs.existsSync(UPLOAD_PATH + savedName) );
    fs.copyFile(file.filepath.toString(), UPLOAD_PATH + savedName + type, err => {if(err) console.log(err);} );
    jimp.read(UPLOAD_PATH + savedName + type, function (err, image) {
        if (err) {
          console.log(err);
        } 
        else {
          image.write(UPLOAD_PATH + savedName + '.bmp');
          fs.unlink(UPLOAD_PATH + savedName + type, err => {if(err) console.log(err)});
          console.log('Image converted');
        }
    });
    return savedName;
};


function deleteUploadedPhoto(file) {
    fs.unlink(UPLOAD_PATH + file + '.bmp', err => {if(err) console.log("File error ", err)});
    //console.log("Photo deleted");
};

module.exports = {moveUploadedPhoto, deleteUploadedPhoto};