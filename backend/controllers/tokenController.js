const {secretKey} = require('../src/config');
const jwt = require('jsonwebtoken');
const crypt = require('../src/crypt');
const { getUserData } = require('./userController');


const generateAccessToken = (id, confirmCode) => {
    const payload = {
        id,
        confirmCode
    }
    return jwt.sign(payload, secretKey);
}

function authenticateToken(req, res, next) {
    const { token } = JSON.parse(crypt.createDecipher(req.body.data));
    if(token.length === 0) return res.sendStatus(401);
    jwt.verify(token, secretKey, (err, user) => {
        if(err) { console.log(err); return res.sendStatus(403); }
        req.user = user;
        next();
    })
}

function authenticateQueryToken(req, res, next) {
    const token = crypt.createDecipher(req.query.token);
    if(token.length === 0) return res.sendStatus(401);
    jwt.verify(token, secretKey, (err, user) => {
        if(err) { console.log(err); return res.sendStatus(403); }
        req.user = user;
        next();
    })
}

function getToken(token) {
    console.log(token);
    //const token = crypt.createDecipher(cipherToken);
    jwt.verify(token, secretKey, (err, user) => {
        if(err) { console.log(err) }
        return user;
    })
}

module.exports = {generateAccessToken, authenticateToken, authenticateQueryToken, getToken};