const crypt = require('../src/crypt')
const db = require('../db/db.js');
const formidable = require('formidable');
const photoController = require('./photoController');
const fs = require('fs');
const jwt = require('jsonwebtoken');

const UPLOAD_PATH = 'pictures/';
const userPhotoTable = 'user_photos';

let returnData;

exports.getUser = async(req, res) => {
    try{
        const userId = req.user.id;

        userExits = await db.user.getUser(userId);
        if(userExits.length == 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'This user not found'}));
            res.status(400).json({data: returnData});
        }
        photoExits = await db.photo.getPhoto(userId, userPhotoTable);

        if(photoExits.length === 0) {
            returnData = JSON.stringify({user: userExits[0], photo: ''});

            console.log('\nUser photo not found\n');
            res.status(200).json({data: returnData});
        }
        else {
            fs.readFile(UPLOAD_PATH + photoExits[0].photo + '.bmp', 'base64', function(err, data){
                if(err) console.log(err);
                
                returnData = crypt.createCipher(JSON.stringify({user: userExits[0], photo: data}));
                res.status(200).json({data: returnData});
            })
            console.log('\nUser data sent\n');
        }
    }catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/', err.message);
    }
}

exports.getUserData = async(req, res) => {
    try{
        const userId = req.user.id;

        userExits = await db.user.getUserById(userId);
        photoExits = await db.photo.getPhoto(userId, userPhotoTable);
        //console.log(userExits[0]);
        //console.log(photoExits[0]);

        if(photoExits.length === 0) {
            returnData = JSON.stringify({user: userExits[0], photo: ''});

            console.log('\nUser photo not found\n');
            res.status(200).json({data: returnData});
        }
        else {
            fs.readFile(UPLOAD_PATH + photoExits[0].photo + '.bmp', 'base64', function(err, data){
                if(err) console.log(err);
                
                returnData = crypt.createCipher(JSON.stringify({user: userExits[0], photo: data}));
                
                //console.log(returnData);
                res.status(200).json({data: returnData});
            })
            console.log('\nUser data sent\n');
        }
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/settings/profile\n', err.message);
    }
};

exports.updateUserName = async(req, res) => {
    try{
        const {name, surname} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;
        
        let userExits = await db.user.getUserById(userId);
        if(userExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'This user not found'}));
            res.status(400).json({data: returnData});
        }
        await db.user.editUser(name, surname, userId);
        returnData = crypt.createCipher(JSON.stringify({message: `User ${userId} has been changed`}));

        res.status(200).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/settings/profile\n', err.message);
    }
};

exports.addUserPhoto = async(req, res) => {
    const formParser = new formidable.IncomingForm();
    formParser.parse(req, async(err, fields, files) => {
        try{
            //console.log(fields);
            //console.log(files.file.filepath);
            const token = jwt.decode(fields.token);
            console.log(token);
            const userId = token.id;
            
            photoExits = await db.photo.getPhoto(userId, userPhotoTable);
            console.log(photoExits);
            if(photoExits.length > 0)
            {
                photoController.deleteUploadedPhoto(photoExits[0].photo);
                await db.photo.deletePhoto(userId, userPhotoTable);
            }
            savedPhoto = photoController.moveUploadedPhoto(files.file, userId);
            console.log(savedPhoto);
            await db.photo.addPhoto(savedPhoto, userId, userPhotoTable);
            
            returnData = crypt.createCipher(JSON.stringify({message: `Photo for user ${userId} has been changed`}));
        
            res.status(200).json({data: returnData});
        } catch(err) {
            returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

            res.status(500).json({data: returnData});
            console.log('\nServer error on /user/settings/profile/photo\n', err.message);
        }
    });
};

exports.updateUserPhone = async(req, res) => {
    try{
        const {phone} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;
        
        let userExits = await db.user.getUserById(userId);
        if(userExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'This user not found'}));
            res.status(400).json({data: returnData});
        }
        await db.user.editPhoneNumber(phone, userId);
        returnData = crypt.createCipher(JSON.stringify({message: `Phone for user ${userId} has been changed`}));
        
        res.status(200).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/settings/profile/phone\n', err.message);
    }
};

exports.updateUserEmail = async(req, res) => {
    try{
        const {email} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;
        
        let userExits = await db.user.getUserById(userId);
        if(userExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'This user not found'}));
            res.status(400).json({data: returnData});
        }
        await db.user.editEmail(email, userId);
        returnData = crypt.createCipher(JSON.stringify({message: `Email for user ${userId} has been changed`}));
        
        res.status(200).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/settings/profile/email\n', err.message);
    }
};

exports.updateUserPassword = async(req, res) => {
    try{
        const {password, oldPassword} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = reqw.user.id;
        
        let userExits = await db.user.getUserById(userId);
        if(userExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'This user not found'}));
            res.status(400).json({data: returnData});
        }
        else if(userExits[0].password === oldPassword) {
            await db.user.editPassword(password, userId);
            returnData = crypt.createCipher(JSON.stringify({message: `Password for user ${userId} has been changed`}));
            res.status(200).json({data: returnData});
        }
        returnData = crypt.createCipher(JSON.stringify({message: 'Access denied'}));
        res.status(400).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/settings/profile/password\n', err.message);
    }
};

exports.getTripsHistory = async(req, res) => {
    try{
        const userId = req.user.id;
        let trips = await db.order.getTripsHistory(userId);
        let history;

        for(let i=0; i < trips.length; i++) {
            if(i === 0)
            {
                history = JSON.stringify(trips[i]);
            }
            else {
                history += JSON.stringify(trips[i]);
            }
        }
        console.log(history + "\n\n");
        returnData = crypt.createCipher(JSON.stringify({history: history}));
        res.status(200).json({data: returnData});
    }catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));
        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/history\n', err.message);
    }
};

exports.addPlace = async(req, res) => {
    try{
        const {address, name} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;
        
        let addressExits = await db.address.getAddressByNameAndUser(address, userId);
        if(addressExits.length === 0) {
            await db.address.editAddress(addressExits[0].id, address, name);
            returnData = crypt.createCipher(JSON.stringify({message: 'Preference place has been added'}));

            res.status(200).json({data: returnData});
        }
        await db.address.editAddress(addressExits[0].id, address, name);
        returnData = crypt.createCipher(JSON.stringify({message: 'Preference place has been added'}));
        
        res.status(200).send({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/places\n', err.message);
    }
};

exports.getPlaces = async(req, res) => {

};

exports.deletePlace = async(req, res) => {
    try{
        const {address} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;
        
        let addressExits = await db.address.getAddressByAddressAndUser(address, userId);
        if(addressExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'Preference place not found'}));
            res.status(400).json({data: returnData});
        }
        await db.address.deleteAddress(addressExits[0].id);
        returnData = crypt.createCipher(JSON.stringify({message: 'Preference place has been deleted'}));

        res.status(200).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/places\n', err.message);
    }
};

exports.addCard = async(req, res) => {
    try{
        const {number, validity, cvv, country} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;

        let cardExits = await db.card.getBankCard(number);

        if(cardExits) {
            returnData = crypt.createCipher(JSON.stringify({message: 'The bank card is already added by this user'}));
            res.status(400).json({data: returnData});
        }

        await db.card.addBankCard(number, validity, cvv, country, userId);
        returnData = crypt.createCipher(JSON.stringify({message: `Bank card has been added by userID ${userId}`}));

        res.status(200).json({data: returnData});

    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/wallet/add\n', err.message);
    }
};

exports.getCards = async(req, res) => {
    try{
        const userId = req.user.id;

        let cardExits = await db.card.getBankCards(userId);

        if(cardExits) {
            returnData = crypt.createCipher(JSON.stringify(json.parse(cardExits)));
            res.status(200).json({data: returnData});
        }
        returnData = crypt.createCipher(JSON.stringify({message: 'No bank cards added yet'}));
        res.status(200).json({data: returnData});

    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/wallet\n', err.message);
    }
};

exports.updateCard = async(req, res) => {
    try{
        const {number, validity, cvv, country} = JSON.parse(crypt.createDecipher(req.body.data));
        const userId = req.user.id;

        let cardExits = await db.card.getBankCard(number);

        if(cardExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'The bank card not found'}));
            res.status(400).json({data: returnData});
        }
        await db.card.editBankCard(cardExits[0].id, number, validity, cvv, country);
        returnData = crypt.createCipher(JSON.stringify({message: `Bank card has been changed by userID ${userId}`}));

        res.status(200).send({data: returnData});
    }catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/wallet/edit\n', err.message);
    }
};

exports.deleteCard = async(req, res) => {
    try{
        const {number} = JSON.parse(crypt.createDecipher(req.body.data));

        let cardExits = await db.card.getBankCard(number);

        if(cardExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'The bank card not found'}));
            res.status(400).json({data: returnData});
        }
        await db.card.deleteBankCard(number);
        returnData = crypt.createCipher(JSON.stringify({message: `Bank card ${number} has been deleted`}));

        res.status(200).send({data: returnData});

    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /user/wallet/edit\n', err.message);
    }
};