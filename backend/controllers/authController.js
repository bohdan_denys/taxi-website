const jwtController = require('../controllers/tokenController');
const cryptoRandString = require('crypto-random-string');
const {sendMail} = require('../src/mailer');
const {sendSMS} = require('../src/smsSender');
const crypt = require('../src/crypt');
const db = require('../db/db.js');

let returnData;

exports.register = async(req, res) => {
    try {
        const {email, name, password, phone, surname} = JSON.parse(crypt.createDecipher(req.body.data));

        let userExits = await db.user.getUserByPhoneNumber(phone);
        console.log(userExits);

        if(userExits.length > 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'The user is already registered by this phone number'}));
            return res.status(400).json({data: returnData})
        }

        const code = cryptoRandString({length: 6, type: 'numeric'});
        await db.user.addConfirmationCode(code, false);
        let userCode = await db.user.getConfirmationCode(code);
        
        const message = {
            to: email,
            subject: 'Register validation',
            text: `Підтвердіть реєстрацію у додатку Zorgo за цією поштою.
            Нікому не передавайте цей код: ${code}
            Співробітники служби допомоги ніколи не запитують паролі.`,
        }
        // Отправка кода подтверждения
        sendMail(message);    
        sendSMS(phone, message.text);

        await db.user.addUser(phone, name, surname, email, crypt.createCipher(password), userCode[0].id);
        userExits = await db.user.getUserByPhoneNumber(phone);     
        await db.rating.addRating(userExits[0].id);

        const token = jwtController.generateAccessToken(userExits[0].id, code);
        returnData = crypt.createCipher(JSON.stringify({message: 'User added', token}));

        res.status(200).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));
        res.status(500).json({data: returnData});
        console.log('\nServer error on /auth/register\n', err.message);
    }
};

exports.login = async(req, res) => {
    try {
        const { phone, password } = JSON.parse(crypt.createDecipher(req.body.data))

        let userExits = await db.user.getUserByPhoneNumber(phone);

        if(userExits.length === 0)
        {
            returnData = crypt.createCipher(JSON.stringify({message: 'User not found'}));
            res.status(401).json({data: returnData});
        }

        if(crypt.createCipher(password) === userExits[0].password)
        {
            const token = jwtController.generateAccessToken(userExits[0].id);
            returnData = crypt.createCipher(JSON.stringify({token: token}));
            //console.log(returnData);
            console.log('\nUser logged\n');

            res.status(200).json({data: returnData});      
        } else {
            returnData = crypt.createCipher(JSON.stringify({message: 'Access denied'}));
            res.status(401).json({data: returnData});
        }
        
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /auth/login\n', err.message);
    }
};

exports.codeConfirm = async(req, res) => {
    try {
        serverCode = req.user.confirmCode;

        if(code != serverCode) {
            returnData = crypt.createCipher(JSON.stringify({message: 'Confirmation code does not match'}));
            res.status(500).json({data: returnData});
        }
        else {
            await db.user.confirmCode(code);
            
            newToken = jwtController.generateAccessToken(tokenData.id);
            returnData = crypt.createCipher(JSON.stringify({token: newToken}));

            res.status(200).json({data: returnData});     
        }
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /auth/confirm\n', err.message);
    }
};