//const jwt = require('../src/tokenController');
const crypt = require('../src/crypt')
const db = require('../db/db.js');
const fs = require('fs');
const cryptoRandString = require('crypto-random-string');

const UPLOAD_PATH = 'pictures/';
const driverPhotoTable = 'driver_photos';

exports.placeOrder = async(req, res) => {
    try{
        //passenger, departure_address, destination_address, taxi_class, payment_method
        const {departure_address, destination_address, taxi_class, payment_method} = JSON.parse(crypt.createDecipher(req.body.data));
        userId = req.user.id;
        orderExits = await db.order.getOrderByUser(userId);

        if(orderExits.length > 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'You already on a trip'}));

            res.status(405).json({data: returnData});
        }
        orderId = cryptoRandString({length: 12, type: 'numeric'});
        await db.order.addOrder(orderId, userId, departure_address, destination_address, taxi_class, payment_method);

        returnData = crypt.createCipher(JSON.stringify({message: 'Your order has been placed', orderId: orderId}));

        // Тимчасовий код для демонстрації
        driverId = 837498439134;
        await db.trip.addTrip(orderId, driverId);
        await db.order.updateOrderStatus('InProgress', orderId);
        console.log('Order accepted');
        // =====================================================

        res.status(200).json({data: returnData});
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /map/order\n', err.message);
    }
};

exports.getDrivers = async(req, res) => {
    try {
        driverExits = await db.driver.getDrivers();
        console.log(driverExits);
        console.log(driverExits.length);
        if(driverExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'Drivers not found'}));
            res.status(400).json({data: returnData});
        }
        for(let i=0; i < driverExits.length; i++) {
            if(i == 0){
                photoData = JSON.stringify({photo: fs.readFileSync(UPLOAD_PATH + driverExits[i].photo + '.bmp', 'base64')});
            }
            else {
                photoData += JSON.stringify({photo: fs.readFileSync(UPLOAD_PATH + driverExits[i].photo + '.bmp', 'base64')});
            }
            //console.log(photoData);
        }
        returnData = JSON.stringify({driver: driverExits, photo: photoData, pricePerMeter: 0.008});
        res.status(200).json({data: returnData});

    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /map/order\n', err.message);
    }
}

exports.getCurrentOrder = async(req, res) => {
    try {
        const {orderId} = JSON.parse(crypt.createDecipher(req.body.data)); // req.query.orderId

        orderExits = await db.order.getOrderById(orderId);

        if(orderExits.length === 0) {
            returnData = crypt.createCipher(JSON.stringify({message: 'Order not found'}));
            res.status(400).json({data: returnData});
        }

        if(orderExits[0].status == 'InProgress') {
            tripExits = await db.trip.getTripByOrder(orderId);
            if(tripExits.length === 0) {
                returnData = crypt.createCipher(JSON.stringify({message: 'Trip not found'}));
                res.status(400).json({data: returnData});
            }
            driverExits = await db.driver.getDriverDataById(tripExits[0].driver);
            photoExits = await db.photo.getPhoto(tripExits[0].driver, driverPhotoTable);
            fs.readFile(UPLOAD_PATH + photoExits[0].photo + '.bmp', 'base64', function(err, data){
                if(err) console.log(err);
                
                //returnData = crypt.createCipher(JSON.stringify({driver: driverExits[0], photo: data, pricePerMeter: '0,008'}));
                returnData = JSON.stringify({driver: driverExits[0], photo: data, pricePerMeter: 0.008});
                res.status(200).json({data: returnData});
            })
            console.log('\nUser data sent\n');
        }
    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /map/order\n', err.message);
    }
};

exports.cancelOrder = async(req, res) => {
    try {

    } catch(err) {
        returnData = crypt.createCipher(JSON.stringify({message: 'Something went wrong, try again later'}));

        res.status(500).json({data: returnData});
        console.log('\nServer error on /map/order\n', err.message);
    }
};