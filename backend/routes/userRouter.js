const { Router } = require('express');
const jwtController = require('../controllers/tokenController');
const userController = require('../controllers/userController');

const router = Router();

router.get('/', jwtController.authenticateToken, userController.getUser);       // authenticateQueryToken               

// api/user/settings/profile
router.get('/settings/profile', jwtController.authenticateQueryToken, userController.getUserData);      // authenticateQueryToken

// api/user/settings/profile
router.put('/settings/profile', jwtController.authenticateToken, userController.updateUserName);      

// api/user/settings/profile/photo
router.post('/settings/profile/photo', userController.addUserPhoto);

// api/user/settings/profile/phone
router.put('/settings/profile/phone', jwtController.authenticateToken, userController.updateUserPhone);

// api/user/settings/profile/email
router.put('/settings/profile/email', jwtController.authenticateToken, userController.updateUserEmail);

// api/user/settings/profile/password
router.put('/settings/profile/password', jwtController.authenticateToken, userController.updateUserPassword);

// api/user/history
router.get('/history', jwtController.authenticateToken, userController.getTripsHistory);        // authenticateQueryToken

// api/user/places
router.post('/places', jwtController.authenticateToken, userController.addPlace);

router.delete('/places', jwtController.authenticateToken, userController.deletePlace);

// api/user//wallet
router.get('/wallet', jwtController.authenticateToken, userController.getCards);                // authenticateQueryToken

router.post('/wallet', jwtController.authenticateToken, userController.addCard);

router.put('/wallet', jwtController.authenticateToken, userController.updateCard);

router.delete('/wallet', jwtController.authenticateToken, userController.deleteCard);


module.exports = router;