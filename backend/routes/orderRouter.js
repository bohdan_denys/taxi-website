const { Router } = require('express');
const jwtController = require('../controllers/tokenController');
const orderController = require('../controllers/orderController');

const router = Router();

/*
Order statuses:

    Expectation
    InProgress
    Cancelled
    Executed
*/

// подумать на роутами еще
// api/map
router.post('/order', jwtController.authenticateToken, orderController.placeOrder);

// Получение предварительной стоимости
// api/map
router.get('/order', jwtController.authenticateToken, orderController.getDrivers);

// Отмена поездки
// api/map
router.delete('/order', jwtController.authenticateToken, orderController.cancelOrder);

module.exports = router;