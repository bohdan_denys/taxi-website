const { Router } = require('express');
const jwtController = require('../controllers/tokenController');
const authController = require('../controllers/authController');

const router = Router();

// api/auth/register
router.post('/register', authController.register);

// api/auth/login
router.post('/login', authController.login);

// api/auth/confirm
router.post('/confirm', jwtController.authenticateToken, authController.codeConfirm);

module.exports = router;