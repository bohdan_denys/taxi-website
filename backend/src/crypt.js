const crypto = require('crypto');
const {secretKey} = require("./config");
const Buffer = require('buffer');

const key = "25CzkNUT246tk864Pbc3YNm32uPuNxkL";
const algorithm = "aes-256-cbc";
//const iv = "31323334353637383930414243444546".slice(0,16);
const iv = "1234567890ABCDEF".slice(0,16);
//31323334353637383930414243444546

function createCipher(data) {
    try {
        const cipher = crypto.createCipheriv(algorithm, key, iv); //Vmesto key budet izna4alnyi secretKey

        let encryptedData = cipher.update(data, 'utf-8', 'base64')
        encryptedData += cipher.final('base64');

        //console.log("Encrypted  data: ", encryptedData);
        return encryptedData;
    } catch(err) {
        console.log(`Server error! The encryption method returned an error ${err}`);
    }
}

function createDecipher(data) {
    try {
        const decipher = crypto.createDecipheriv(algorithm, key, iv);

        let decryptedData = decipher.update(data, 'base64', 'utf-8')
        decryptedData += decipher.final('utf-8');
        
        console.log("Decrypted data: ", decryptedData);
        return decryptedData;
    } catch(err)
    {
        console.log(`Server error! The decryption method returned an error ${err}`);
    }
    
}



module.exports = {createCipher, createDecipher};