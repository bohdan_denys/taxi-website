module.exports = {
    
    General: {
        rushHour: 10,
        animal: 20,
        pricePerMinute: 2

    },
    Economy: {
        fixPrice: 50,                                             // Установленная стоимость для типа такси
        exactDistance: 2,                                         // Максимальное расстояние за установленную цену
        pricePerCilometer: 7,                                     // Цена за километр 
        freeWaiting: 5,                                           // Бесплатное время ожидания
        pricePerMinute: this.General.pricePerMinute,              // Цена за минуту ожидания после того, как время превысит бесплатный лимит. (Так же срабатывает в пробках)*
        animal: this.General.animal                               // Надбавка за перевозку животного (возможно добавить уточнение про размер животного)
    }, 
    Standard: {
        fixPrice: 60,           
        exactDistance: 2,
        pricePerCilometer: 9,
        freeWaiting: 5,
        pricePerMinute: this.General.pricePerMinute,
        animal: this.General.animal   
    },
    Comfort: {
        fixPrice: 70,           
        exactDistance: 2,
        pricePerCilometer: 9,
        freeWaiting: 5,
        pricePerMinute: this.General.pricePerMinute,
        animal: this.General.animal     
    },
    StationWagon: {
        fixPrice: 80,           
        exactDistance: 2,
        pricePerCilometer: 9,
        freeWaiting: 5,
        pricePerMinute: this.General.pricePerMinute,
        animal: this.General.animal     
    },
    Business: {
        fixPrice: 120,           
        exactDistance: 2,
        pricePerCilometer: 10,
        freeWaiting: 5,
        pricePerMinute: this.General.pricePerMinute,
        animal: this.General.animal + 10     
    }
}