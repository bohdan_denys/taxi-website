const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport(
    {
        host: 'smtp.ukr.net',
        port: 465,
        secure: true,
        auth: {
            user: 'taxismtptest@ukr.net',
            pass: 'h49p8ob48V74q12N'
        },
        tls: {
            rejectUnauthorized: false
        }
    },
    {
        from: 'Zorgo <taxismtptest@ukr.net>',
    }
)

const sendMail = message => {
    transporter.sendMail(message, (err, info) => {
       if(err) return console.log(err)
       console.log('Email sent: ', info);
    })
}

module.exports = {sendMail};