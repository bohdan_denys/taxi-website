const config = require('./costConfig');


function calculationProbableCost(taxiType, distance, animal) {
    let price = 0;

    switch(taxiType) {
        case "Economy": 
            price = config.Economy.fixPrice + (config.Economy.pricePerCilometer * (distance - config.Economy.exactDistance));
            if(animal) price += config.Economy.animal;

            return price;
        case "Standart": 
            price = config.Standart.fixPrice + (config.Standart.pricePerCilometer * (distance - config.Standart.exactDistance));
            if(animal) price += config.Standart.animal;

            return price;
        case "Comfort": 
            price = config.Comfort.fixPrice + (config.Comfort.pricePerCilometer * (distance - config.Comfort.exactDistance));
            if(animal) price += config.Comfort.animal;
            
            return price;
        case "StationWagon": 
            price = config.StationWagon.fixPrice + (config.StationWagon.pricePerCilometer * (distance - config.StationWagon.exactDistance));
            if(animal) price += config.StationWagon.animal;

            return price;
        case "Business": 
            price = config.Business.fixPrice + (config.Business.pricePerCilometer * (distance - config.Business.exactDistance));
            if(animal) price += config.Business.animal;

            return price;
    }
};

function caclculationTripPrice() {

};

module.exports = {calculationProbableCost};