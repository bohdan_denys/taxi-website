let registration_step;

let phone_form = `<div>
    <h5>Введите свой номер телефона (обязательно)</h5>
    <div class="wide"><input type="tel" id="phone" ></div>
    <button id="next">Далее</button>
    <p>Продолжая, вы утверждаете, что прочитали и принимаете 
    <a href="some.html">Или подключитесь с помощью аккаунта социальной сети</a> 
</div>`;

let name_form = `<div>
    <h5>Введите данные для создания аккаунта</h5>

    <div class="row">
        <div class="input_container">
            <p>Имя(обязательно)</p>
            <input type="text" placeholder="Имя"  id="first_name">
        </div>

        <div class="input_container">
            <p>Фамилия(обязательно)</p>
            <input type="text" placeholder="Фамилия" id="second_name">
        </div>
    </div>
   

    <div class="input_container">
        <p>Введите пароль(обязательно)</p>
        <input type="password" id="pass">
    </div>
    <button id="next">Далее</button>
    <p>Продолжая, вы утверждаете, что прочитали и принимаете 
    <a href="some.html">Условия предсталения услуг</a> и <a href="some.html">Политику конфинденциальности</a>.
    </p>
</div>`;

let email_form = `<div>
    <h5>Укажите ваш адрес эл. почты</h5>
    <div class="wide">
        <p>Введите свой адрес электронной почты(обязательно)</p>
        <input type="email" id="email" >
    </div>
    <button id="next">Далее</button>
</div>`;

let code_form = `<div>
    <h5>На вашу почту отправлен код подтверждения</h5>
    <div class="wide"><input type="tel" id="code" ></div>
    <button id="next">Далее</button>
</div>`;

let _code_input_confirm = "Код введён неправильно";
let _code_is_wrong;

document.addEventListener("DOMContentLoaded", function(){
	registration_step = 1;
    
    document.getElementById("registerform").innerHTML = phone_form;
    document.getElementById("next").addEventListener("click", next_click);
    document.getElementById("phone").addEventListener('input', validatePhone);
});

let phone;
let first_name;
let second_name;
let password;
let email;
let code;

function next_click(e){
    switch(registration_step)
    {
        //phone input step
        case 1:
            registration_step += 1;

            phone = document.getElementById("phone").value;

            document.getElementById("registerform").innerHTML = name_form;
            document.getElementById("next").addEventListener("click", next_click);

            document.getElementById("pass").addEventListener('input', validatePassword);
            //document.getElementById("email").addEventListener('input', validateEmail);
            //document.getElementById("next").addEventListener("click", next_click);
            break;

        //name, email, pass
        case 2:
            registration_step += 1;

            first_name = document.getElementById("first_name").value;
            second_name = document.getElementById("second_name").value;
            password = document.getElementById("pass").value;
            document.getElementById("next").addEventListener("click", next_click);

            document.getElementById("registerform").innerHTML = email_form;
            document.getElementById("email").addEventListener('input', validateEmail);
            document.getElementById("next").addEventListener("click", next_click);
            break;

        //email
        case 3:
           
            registration_step += 1;
            email = document.getElementById("email").value;

            //sending form to server
            send_register_form();

            document.getElementById("registerform").innerHTML = code_form;
            document.getElementById("code").addEventListener('input', validateCode);
            document.getElementById("next").addEventListener("click", next_click);

            break;
        
        //verification code
        case 4:
            let resp = send_code();

            if(resp == true){
                registration_step += 1;
                
            }
            else{
                alert(_code_is_wrong);
                document.getElementById("registerform").innerHTML = code_form;
                document.getElementById("code").addEventListener('input', validateCode);
                document.getElementById("next").addEventListener("click", next_click);
            }

            break;

        default:
            break;
    }
}

function send_form_test(){
    document.body
    .innerHTML = `
        <div class="col">
        <p>phone ${phone}</p>
        <p>email ${email}</p>
        <p>first name ${first_name}</p>
        <p>second name ${second_name}</p>
        <p>password ${password}</p>
        </div>
    `;

}

// Validate email
function validateEmail(e) {
   
    let email = e.currentTarget.value;
    let re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if(re.test(String(email).toLowerCase()) == true){
        document.getElementById("next").disabled = false;
        e.currentTarget.style.border = "0px";
        e.currentTarget.style.borderBottom = "1px solid lightslategray";
    }
    else{
        document.getElementById("next").disabled = true;
        e.currentTarget.style.border = "1px solid red";
        e.currentTarget.style.borderRadius = "3px";
    }
}

function validateCode(e) {
    
    if(e.currentTarget.value.replace(/\s/g, '').length == 6){
        code = e.currentTarget.value.replace(/\s/g, '');
    }
    else{
        document.getElementsByTagName("h5")[0].textContent = _code_input_confirm ;
    }
}

function validatePhone(e) {
    let phone = e.currentTarget.value;
    let re = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;

    if(re.test(phone) == true){
        document.getElementById("next").disabled = false;
        e.currentTarget.style.border = "0px";
        e.currentTarget.style.borderBottom = "1px solid lightslategray";
    }
    else{
        document.getElementById("next").disabled = true;
        e.currentTarget.style.border = "1px solid red";
        e.currentTarget.style.borderRadius = "3px";
    }
    
}

function validatePassword(e){
    let pass = e.currentTarget.value;
    //let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\w\s]).{6,}/;
    let re = /(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/g;
    if(re.test(pass) == true){
        document.getElementById("next").disabled = false;

        e.currentTarget.style.border = "0px";
        e.currentTarget.style.borderBottom = "1px solid lightslategray";
        e.currentTarget.style.borderRadius = "3px";

    }
    else{
        document.getElementById("next").disabled = true;
       
        e.currentTarget.style.border = "1px solid red";
        e.currentTarget.style.borderRadius = "3px";
    }
}

async function send_register_form(){
    phone = phone.replace(/[^0-9]/g, '');
    let data = {
        phone : phone,
        email : email,
        first_name : first_name,
        second_name : second_name,
        password : password
    }
    
    
    try {
        const response = await fetch('/api/auth/register', {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          }
        });
        const json = await response.json();
        console.log(data);
        console.log('Data send succes :', JSON.stringify(json));
      } catch (error) {
        console.error('Data send fail, error:', error);
    }
}

async function send_code(){
    let resp;
    try {
        const response = await fetch('api/auth/confirm', {
          method: 'POST',
          body: JSON.stringify(code),
          headers: {
            'Content-Type': 'application/json'
          }
        });
        resp = await response.json();
        console.log(data);
        console.log('Data send succes :', JSON.stringify(json));
        return resp;
    } 
    catch (error) {
        console.error('Data send fail, error:', error);
    }
}


