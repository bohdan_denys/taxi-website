const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');

/*
Order statuses:

    Expectation
    InProgress
    Cancelled
    Executed
*/

//#region Orders

function addOrder(id, passenger, departure_address, destination_address, taxi_class, payment_method, status) {
    //const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO orders (id, passenger, departure_address, destination_address, date, taxi_class, payment_method, status) VALUES (?, ?, ?, ?, NOW(), ?, ?, ?)';
    const data = [id, passenger, departure_address, destination_address, taxi_class, payment_method, status];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nOrder has been created\n');

            resolve(results);
        })
    })
}

function createReservation(passenger, departure_address, destination_address, taxi_class, payment_method, date) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const status = 'Reserved';
    const sql = 'INSERT INTO orders (id, passenger, departure_address, destination_address, date, taxi_class, payment_method, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
    const data = [id, passenger, departure_address, destination_address, date, taxi_class, payment_method, status];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nOrder has been created and planned\n');

            resolve(results);
        })
    })
}

function getOrders(user) {
    const sql = 'SELECT * FROM orders WHERE user = ?';
    const data = [user];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}

function getOrderById(id) {
    const sql = 'SELECT * FROM orders WHERE id = ?';
    const data = [id];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}

function getOrderByUser(userId) {
    const sql = `SELECT * FROM orders WHERE (status = 'Expectation' OR status = 'InProgress') and passenger = ?`;
    const data = [userId];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}

function updateOrderStatus(status, id) {
    const sql = 'UPDATE orders SET status = ? WHERE id = ?';
    const data = [status, id];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nOrder status has been updated\n');

            resolve(results);
        })
    })
}
//#endregion

function getTripsHistory(userId) {
    const sql = 'SELECT  o.`date`, o.payment_method, o.status, o.departure_address, o.destination_address, t.cost, t.driver_rating, t.duration, t.distance'
    + ' FROM orders o, trips t'
    + ' WHERE t.custom = o.id AND o.passenger = ?';
    const data = [userId];

    return new Promise((resolve, reject) =>{
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nThe history of trips is received\n');

            resolve(results);
        })
    })
}

module.exports = {addOrder, getOrders, getOrderById, getOrderByUser, updateOrderStatus, getTripsHistory};