const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');

function addRating(userId){
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO user_ratings (id, user, value) VALUES (?, ?, 0.0)';
    const data = [id, userId]; 

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);
			resolve(results);
        })
    })
}
module.exports = {addRating};