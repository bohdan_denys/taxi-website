const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');


function addBankCard(number, validity, cvv, country, owner) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO bank_cards (id, number, validity, cvv, country, owner) VALUES (?, ?, ?, ?, ?, ?)';
    const data = [id, number, validity, cvv, country, owner];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

			console.log(`\nBank card has been added\n`);

			resolve(results);
        })
    })
}

function getBankCardByNumber(number) {
    const sql = 'SELECT * FROM bank_cards WHERE number = ?';
    const data = [number];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}

function getBankCards(owner) {
    const sql = 'SELECT * FROM bank_cards WHERE owner = ?';
    const data = [owner];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}

function editBankCard(id, number, validity, cvv) {
    const sql = 'UPDATE bank_cards SET number = ?, validity = ?, cvv = ?, WHERE id = ?';
    const data = [number, validity, cvv, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nCard information has been changed\n');

            resolve(results);
        })
    })
}

function deleteBankCard(number) {
    const sql = 'DELETE FROM bank_cards WHERE number = ?';
    const data = [number];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nBank card has been deleted\n');

            resolve(results);
        })
    })
}


module.exports = {addBankCard, getBankCardByNumber, getBankCards, editBankCard, deleteBankCard};