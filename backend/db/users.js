const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');

//#region User
function addUser(phone, name, surname, email, password, userCode) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO users (id, phone, name, surname, email, password, confirmed) VALUES (?, ?, ?, ?, ?, ?, ?)';
    const data = [id, phone, name, surname, email, password, userCode];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

			console.log(`Inserted user with phone: ${phone}`);

			resolve(results);
        })
    })
}
// USE Taxi
// INSERT INTO users (id, phone, name, surname, email, password, confirmed) VALUES (UUID_SHORT(), '+380502111618', 'Денис', 'Демиров', 'denis216051@gmail.com', 'Pass2@', '99790840183914513')

function getUserByPhoneNumber(phone) {
    const sql = 'SELECT * FROM users WHERE phone=?';
    const data = [phone];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        });
    });
}

function getUserById(id) {
    const sql = 'SELECT * FROM users WHERE id = ?';
    const data = [id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        });
    });
}

function getUser(id){
    const sql = 'SELECT u.name, u.surname, ur.value AS rating'
    + ' FROM users u, user_ratings ur'
    + ' WHERE u.id = ?';
    const data = [id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        });
    });
}
//#endregion

//#region Profile Settings
function editUser(name, surname, id) {
    const sql = 'UPDATE users SET name = ?, surname = ? WHERE id = ?';
    const data = [name, surname, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            console.log(`\nUser ${id} has been changed\n`);
            resolve(results);
        })
    })
}

function editPhoneNumber(phone, id) {
    const sql = 'UPDATE users SET phone = ? WHERE id = ?';
    const data = [phone, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            console.log(`\nPhone number by user ${id} has been changed\n`);
            resolve(results);
        })
    })
}

function editEmail(email, id) {
    const sql = 'UPDATE users SET email = ? WHERE id = ?';
    const data = [email, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            console.log(`\nEmail by user ${id} has been changed\n`);
            resolve(results);
        })
    })
}

function editPassword(password, id) {
    const sql = 'UPDATE users SET password = ? WHERE id = ?';
    const data = [password, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            console.log(`\nPassword by user ${id} has been changed\n`);
            resolve(results);
        })
    })
}

function editLanguage(language, id) {
    const sql = 'UPDATE users SET language = ? WHERE id = ?';
    const data = [language, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            console.log(`\nLanguage by user ${id} has been changed\n`);
            resolve(results);
        })
    })
}


//#endregion

//#region Confirmation Codes
function addConfirmationCode(code, confirmed) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO confirmation_codes (id, code, confirmed, created_at) VALUES (?, ?, ?, NOW())';
    const data = [id, code, confirmed];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);
            
            console.log(`Verification code created\n`);
            
            resolve(results);
        })
    })
}

function confirmCode(code) {
    const sql = 'UPDATE confirmation_codes SET confirmed = true WHERE code = ?';
    const data = [code];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('Verification code confirmed\n');

            resolve(results);
        })
    })
}

function getConfirmationCode(code) {
    const sql = 'SELECT * FROM confirmation_codes WHERE code = ?';
    const data = [code];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}
//#endregion

module.exports = {addUser, getUserByPhoneNumber, getUserById, getUser, editUser, editPhoneNumber, editEmail, editPassword, editLanguage, addConfirmationCode, confirmCode, getConfirmationCode};