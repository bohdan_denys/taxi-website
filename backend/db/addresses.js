const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');


function addAddress(address, name, user) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO saved_places (id, address, name, user) VALUES (?, ?, ?, ?)';
    const data = [id, address, name, user];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

			console.log(`\nAddress has been added\n`);

			resolve(results);
        })
    })
}

function getAddressByAddressAndUser(address, user) {
    const sql = 'SELECT * FROM saved_places WHERE address = ?, user = ?';
    const data = [address, user];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            resolve(results);
        })
    })
}

function editAddress(id, address, name) {
    const sql = 'UPDATE saved_places SET address = ?, name = ?, WHERE id = ?';
    const data = [address, name, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nAddress has been changed\n');

            resolve(results);
        })
    })
}

function deleteAddress(id) {
    const sql = 'DELETE FROM saved_places WHERE number = ?';
    const data = [id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);

            console.log('\nAddress has been deleted\n');

            resolve(results);
        })
    })
}

module.exports = {addAddress, getAddressByAddressAndUser, editAddress, deleteAddress};