const mysql = require('mysql2');

const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'taxi_user',
    password: 'taxi_pass',
    database: 'Taxi'
});

module.exports = pool;