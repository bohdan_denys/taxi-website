const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');
const crypt = require('../src/crypt');

//#region User Photos

function addPhoto(photo, owner, table) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO ' + table + ' (id, photo, owner) VALUES (?, ?, ?)';
    const data = [id, photo, owner];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            console.log("\nUser photo added\n");

            resolve(results);
        });
    });
};

function getPhoto(owner, table) {
    const sql = 'SELECT photo FROM ' + table + ' WHERE owner = ?';
    const data = [owner];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);

            resolve(results);
        });
    });
}

function deletePhoto(owner, table) {
    const sql = 'DELETE FROM ' + table + ' WHERE owner = ?';
    const data = [owner];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results) {
            if(err) reject(err);
            
            console.log('\nUser photo deleted\n');
            resolve(results);
        });
    });
}
//#endregion

module.exports = {addPhoto, getPhoto, deletePhoto};