const user = require('../db/users');
const order = require('../db/orders');
const card = require('../db/cards');
const address = require('../db/addresses');
const photo = require('../db/photos');
const trip = require('../db/trips');
const driver = require('../db/drivers');
const rating = require('../db/ratings');

const db = {
    user: user,
    order: order,
    card: card,
    address: address,
    photo: photo,
    trip: trip,
    driver: driver,
    rating: rating
}

module.exports = db;