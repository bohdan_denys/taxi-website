const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');

function addTrip(custom, driver) {
    const id = cryptoRandString({length: 12, type: 'numeric'});
    const sql = 'INSERT INTO trips(id, custom, driver) VALUES (?, ?, ?)';
    const data = [id, custom, driver];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);
            console.log('Trip added');
            resolve(results);
        });
    });
};

function getTripByOrder(custom) {
    const sql = 'SELECT * FROM trips WHERE custom = ?';
    const data = [custom];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);
            resolve(results);
        });
    });
};

function updateTripInfo(id, cost, duration, distance) {
    const sql = 'UPDATE trips SET cost = ?, duration = ?, distance = ? WHERE id = ?';
    const data = [cost, duration, distance, id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);
            console.log(`${id} trip information updated`);
            resolve(results);
        });
    });
};

module.exports = {addTrip, getTripByOrder, updateTripInfo};