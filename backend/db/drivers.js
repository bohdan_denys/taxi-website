const pool = require('./pool');
const cryptoRandString = require('crypto-random-string');

function getDriverDataById(id) {
    const sql = 'SELECT d.name, d.surname, d.phone, dr.value AS rating, c.model, c.plate_numbers, t.name AS class'
    + ' FROM drivers d'
    + ' JOIN cars c'
    + ' ON d.car = c.id'
    + ' JOIN taxi_classes t' 
    + ' ON c.taxi_class = t.id'
    + ' JOIN driver_ratings dr'
    + ' ON dr.driver = d.id'
    + ' WHERE d.id = ?';
    const data = [id];

    return new Promise((resolve, reject) => {
        pool.query(sql, data, function(err, results){
            if(err) reject(err);
            resolve(results);
        });
    });
};

function getDrivers() {
    const sql = 'SELECT d.name, d.surname, d.phone, p.photo, dr.value AS rating, c.model, c.plate_numbers, t.name AS class'
    + ' FROM drivers d'
    + ' JOIN cars c'
    + ' ON d.car = c.id'
    + ' JOIN taxi_classes t' 
    + ' ON c.taxi_class = t.id'
    + ' JOIN driver_ratings dr'
    + ' ON dr.driver = d.id'
    + ' JOIN driver_photos p'
    + ' ON p.owner = d.id';

    return new Promise((resolve, reject) => {
        pool.query(sql, function(err, results){
            if(err) reject(err);
            resolve(results);
        });
    });
}

module.exports = {getDriverDataById, getDrivers};