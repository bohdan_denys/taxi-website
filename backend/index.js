const express = require('express');
const https = require('https');
const fs = require('fs');
const path = require('path');
const cors = require('cors');

const app = express();

const host = '127.0.0.1';
const port = 5000;

//Настройка corse
const corsOptions = {
	origin: 'http://localhost:4200', // домен сервиса, с которого будут приниматься запросы
	optionsSuccessStatus: 200 // для старых браузеров
}

// httpsOptions = {
//     key: fs.readFileSync("server.key"), // путь к ключу
//     cert: fs.readFileSync("server.crt") // путь к сертификату
// }

app.use(cors(corsOptions)); // если не указать corsOptions, то запросы смогут слать все запросы
  

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/api/auth', require('./routes/authRouter'));
app.use('/api/user', require('./routes/userRouter'));
app.use('/api/map', require('./routes/orderRouter'));

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
	res.send('<h1>Hello Taxi!</h1>');
});

app.get('/uber.png', (req, res) => {
	res.type('png');
	res.sendFile(path.join(__dirname, './views/components/uber.png'));
	console.log("sended file " + path.join(__dirname, './views/components/uber.png'))
});

app.get('/register', (req, res) => {
	
	res.sendFile(path.join(__dirname, './views/register.html'));
});

// https.createServer(
//     {
//       key: fs.readFileSync('/backend/cert/key.pem'),
//       cert: fs.readFileSync('/backend/cert/cert.pem'),
//     },
//     app
//   )
//   .listen(port, host, function () {
//     console.log(`Example app listening at https://${host}:${port}`);
//   });

//https.createServer(httpsOptions, app).listen(443);
app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}\n`);
});