import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register/register.component';
import { MainpageComponent } from './mainpage/mainpage/mainpage.component';

const routes: Routes = [
  {path: '', component: MainpageComponent},
  {path: 'home', component: MainpageComponent},
  {path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
