import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders }   from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  @ViewChild('nextBtn')     nextBtn: any;
  @ViewChild('email')       email: any;
  @ViewChild('password')    password: any;
  @ViewChild('first_name')  first_name: any;
  @ViewChild('second_name') second_name: any;
  @ViewChild('phone')       phone: any;
  @ViewChild('code')        code: any;
  @ViewChild('infoErr')     infoErr: any;

  constructor(private http: HttpClient) { }

  registration_step: number = 1;
  _phone:       string = "";
  _first_name:  string = "";
  _second_name: string = "";
  _password:    string = "";
  _email:       string = "";
  _code:        string = "";

  REGISTRATION_URL:           string ='http://localhost:5000/api/auth/register';
  REGISTRATION_CONFIRM_URL:   string ='http://localhost:5000/api/auth/confirm';

  is_Send_ok: boolean = false;


  ngOnInit(): void {
  }

  next(): void {
    switch(this.registration_step)
    {
        //phone input step
        case 1:
            this.registration_step += 1;
            this._phone = this.phone.nativeElement.value;
            console.log(this.registration_step);
            break;

        //name, email, pass
        case 2:
            this.registration_step += 1;

            this._first_name = this.first_name.nativeElement.value;
            this._second_name = this.second_name.nativeElement.value;
            this._password = this.password.nativeElement.value;

            console.log(this.registration_step);
            break;

        //email
        case 3:
            this.registration_step += 1;
            this._email = this.email.nativeElement.value;
            console.log(this.registration_step);

            this.send_register_form();
            break;

        //verification code
        case 4:
            console.log(this.registration_step);
            this._code = this.code.nativeElement.value;
            this.sen_code();
            if(this.is_Send_ok == true){
              this.registration_step += 1;
            }
            else{
              alert("Ошибка");
            }
            break;

        default:
            break;
    }
  }

  // Validate email
  validateEmail(): void {

    this._email = this.email.nativeElement.value;
    console.log(this._email);
    let re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (re.test(String(this._email).toLowerCase()) == true) {
      this.nextBtn.nativeElement.disabled = false;
      this.email.nativeElement.style.border = "0px";
      this.email.nativeElement.borderBottom = "1px solid lightslategray";

      this.hideErr();
    }
    else {
      this.nextBtn.nativeElement.disabled = true;
      this.email.nativeElement.border = "1px solid red";
      this.email.nativeElement.borderRadius = "3px";
      this.showErr("Некорректно ввёденный email");
    }
  }

  
  validateCode(): void {

    if(this.code.nativeElement.value.replace(/\s/g, '').length == 6){
      this._code = this.code.nativeElement.value.replace(/\s/g, '');
      console.log(this._code);
      this.hideErr();
    }
    else{
      document.getElementsByTagName("h5")[0].textContent = "Коды совпадают, вы зарегистрированы" ;
      this.showErr("Некорректно ввёденный код. Код состоит из 6 символов");
    }
  }

  validatePhone(): void {
    console.log(this._phone);
    this._phone = this.phone.nativeElement.value;
    let re = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;

    if(re.test(this._phone) == true){
        this.nextBtn.nativeElement.disabled = false;
        this.phone.nativeElement.style.border = "0px";
        this.phone.nativeElement.style.borderBottom = "1px solid lightslategray";
        this.hideErr();
    }
    else{
        this.nextBtn.nativeElement.disabled = true;
        this.phone.nativeElement.style.border = "1px solid red";
        this.phone.nativeElement.style.borderRadius = "3px";
        this.showErr("Введённый номер не является телефонным номером");
    }

  }

  validatePassword():void{
    console.log(this._password);
    this._password= this.password.nativeElement.value;
    //let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\w\s]).{6,}/;
    let re = /(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/g;
    if(re.test(this._password) == true){
        this.nextBtn.nativeElement.disabled = false;

        this.password.nativeElement.style.border = "0px";
        this.password.nativeElement.style.borderBottom = "1px solid lightslategray";
        this.password.nativeElement.style.borderRadius = "3px";
        this.hideErr();

    }
    else{
        this.nextBtn.nativeElement.disabled = true;
        this.password.nativeElement.style.border = "1px solid red";
        this.password.nativeElement.style.borderRadius = "3px";
        this.showErr("Пароль должен состоять из 6 символов и содержать 1 цифру, символ и заглавную букву")
    }
  }

  //show error text in infoErr element
  showErr(err:string):void{
    if(this.infoErr.nativeElement.style.display == "none"){
      this.infoErr.nativeElement.style.display = "flex";
      this.infoErr.nativeElement.textContent = err;
    }
    else{
      this.infoErr.nativeElement.textContent = err;
    }
  }
  hideErr():void{
    if(this.infoErr.nativeElement.style.display == "none"){
    }
    else{
      this.infoErr.nativeElement.style.display = "none"
    }
  }


  async send_register_form(){
    console.log("Sending form...");
    this._phone = this._phone.replace(/[^0-9]/g, '');
    let json;
    let data = {
        phone : this._phone,
        email : this._email,
        first_name : this._first_name,
        second_name : this._second_name,
        password : this._password
    }


    try {
        const response = await fetch(this.REGISTRATION_URL, {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          }
        });

        json = await response.json();

        console.log(data);
        console.log('Data send succes :', JSON.stringify(json));
        this.is_Send_ok = true;
      } catch (error) {
        console.error('Data send fail, error:', error);
        this.is_Send_ok = false;
    }
  }
  async sen_code(){
    console.log("Sending code...");
    let resp;
    try {
        const response = await fetch(this.REGISTRATION_CONFIRM_URL, {
          method: 'POST',
          body: JSON.stringify(this._code),
          headers: {
            'Content-Type': 'application/json'
          }
        });
        resp = await response.json();
        console.log(resp);
        console.log('Data send succes :', resp);
        this.is_Send_ok = true;
    }
    catch (error) {
        console.error('Data send fail, error:', error);
        this.is_Send_ok = false;
    }
  }

  send_form():void{

    let data = {
      phone         : this._phone,
      email         : this._email,
      first_name    : this._first_name,
      second_name   : this._second_name,
      password      : this._password
    }
    console.log("Sending form...");
    console.log(data);
    console.log(this.REGISTRATION_URL);

    //create header
    let header = new HttpHeaders().set( 'Content-Type', 'application/json');

    //post request
    this.http.post(
      this.REGISTRATION_URL,
      JSON.stringify(data),
      {headers:header}
    );
  }

  send_code():boolean{

    console.log("Sending code...");
    console.log(this._code);
    console.log(this.REGISTRATION_CONFIRM_URL);
    let resp:boolean = false;
    //create header
    let header = new HttpHeaders().set( 'Content-Type', 'application/json');

    //post request
    this.http.post(
      this.REGISTRATION_CONFIRM_URL,
      JSON.stringify(this._code),
      {headers:header}
    ).subscribe((data:any) => {resp = data} );
    return resp;
  }
}

